package com.example.restfull_HW;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfullHwApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfullHwApplication.class, args);
	}

}
