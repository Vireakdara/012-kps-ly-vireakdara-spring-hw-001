package com.example.restfull_HW.respository;

import com.example.restfull_HW.respository.Model.Book;
import com.example.restfull_HW.respository.Model.Category;
import com.example.restfull_HW.respository.Provider.BookProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {
    @InsertProvider(type = BookProvider.class,method = "insertBookSql")
    boolean insert(Book bookModel);
    @SelectProvider(type = BookProvider.class,method = "SelectAllBookSql")

    List<Book> select();

    @SelectProvider(type = BookProvider.class,method = "selectCategoryByIdSql")
    List<Book> selectCategoryById(int id);


    @DeleteProvider(type = BookProvider.class, method = "DeleteBookByIdSql")
    int DeleteBook(int id);

    @UpdateProvider(type = BookProvider.class,method = "UpdateBookByIdSql")
    int UpdateBookById(int id , Book bookModel);

    @SelectProvider(type = BookProvider.class,method = "SelectFilterByBookTitle")
    List<Book> filterByTitle(String title);

    @SelectProvider(type = BookProvider.class,method = "FindOneByID")
    List<Book> FindById(int id);

}
