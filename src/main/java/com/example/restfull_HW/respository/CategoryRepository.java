package com.example.restfull_HW.respository;

import com.example.restfull_HW.respository.Model.Category;
import com.example.restfull_HW.respository.Provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @InsertProvider(type = CategoryProvider.class,method = "InsertCategory")
    boolean insert(Category category);
    @SelectProvider(type = CategoryProvider.class,method = "SelectCategory")
    @Results({
            @Result(property = "id",column = "category_id")
    })
    List<Category> select();

    @DeleteProvider(type = CategoryProvider.class,method = "DeleteCategory")
    int DeletedCategoryById(int id);

    @UpdateProvider(type = CategoryProvider.class,method = "UpdateCategory")
    int UpdateCategory(int id,Category category);

}
