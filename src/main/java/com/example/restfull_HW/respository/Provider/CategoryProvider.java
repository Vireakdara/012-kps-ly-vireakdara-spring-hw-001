package com.example.restfull_HW.respository.Provider;

import com.example.restfull_HW.respository.Model.Category;
import org.apache.ibatis.jdbc.SQL;


public class CategoryProvider {
    public String InsertCategory(){
        return new SQL(){{
            INSERT_INTO("tb_categories");
            VALUES("title", "#{title}");
        }}.toString();
    }
    public String SelectCategory(){
        return  new SQL(){{
            SELECT("*");
            FROM("tb_categories");
        }}.toString();
    }
    public String DeleteCategory(int id){
        return new SQL(){{
            DELETE_FROM("tb_categories");
            if(id>0)
                WHERE("id=#{id}");
        }}.toString();

    }
    public String UpdateCategory(int id, Category category){
        return new SQL(){{
            UPDATE("tb_categories");
            SET("title=#{category.title}");
            WHERE("id=#{id}");
        }}.toString();
    }
}
