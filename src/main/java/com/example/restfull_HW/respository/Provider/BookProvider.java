package com.example.restfull_HW.respository.Provider;

import com.example.restfull_HW.respository.Model.Book;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
   public String insertBookSql(){
       return new SQL(){{
           INSERT_INTO("tb_books");
           VALUES("title", "#{title}");
           VALUES("author", "#{author}");
           VALUES("description", "#{description}");
           VALUES("category_id", "#{category.id}");

       }}.toString();
   }
    public String UpdateBookByIdSql(int id, Book book){
        return new SQL(){{
            UPDATE("tb_books");
            SET("title=#{book.title}", "author=#{book.author}", "description=#{book.description}", "category_id=#{book.category.id}");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String selectCategoryByIdSql(int id){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books b");
            INNER_JOIN("tb_categories c ON b.category_id = c.category_id");
            WHERE("c.category_id = #{id}");
        }}.toString();
    }

    public String DeleteBookByIdSql(int id){
        return new SQL(){{
            DELETE_FROM("tb_books");
            if(id>0)
                WHERE("id=#{id}");
        }}.toString();
    }

    public String SelectAllBookSql(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }

    public String FindOneByID(int id){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("tb_books.id=#{id}");
        }}.toString();
    }

    public String SelectFilterByBookTitle(String title){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");

            if(title!=null || !title.equals(""))
                WHERE("title like '%"+title+"%' ");
        }}.toString();
    }

}
