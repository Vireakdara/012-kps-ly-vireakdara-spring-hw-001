package com.example.restfull_HW.respository.Model;

public class Category {
    private int id;
    private String title;

    public Category(){

    }

    public Category(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoriesModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
