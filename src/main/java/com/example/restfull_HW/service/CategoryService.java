package com.example.restfull_HW.service;

import com.example.restfull_HW.respository.Model.Category;

import java.util.List;

public interface CategoryService {
    Category insert(Category category);
    List<Category> select();
    int DeletedCategoryById(int id);
    int UpdateCategory(int id, Category category);
}
