package com.example.restfull_HW.service;

import com.example.restfull_HW.respository.Model.Book;

import java.util.List;

public interface BookService {
    Book insert(Book bookModel);
    List<Book> select();
    int DeleteBook(int id);
    int UpdateBookById(int id,Book bookModel);
    List<Book> filterByTitle(String title);
    List<Book> FindById(int id);
}
