package com.example.restfull_HW.service.Imp;

import com.example.restfull_HW.respository.CategoryRepository;
import com.example.restfull_HW.respository.Model.Category;
import com.example.restfull_HW.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    private CategoryRepository categoryRepository ;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category insert(Category category) {
        boolean isInserted  = categoryRepository.insert(category);
        if(isInserted){
            return category;
        }else{
            return null;
        }
    }

    @Override
    public List<Category> select() {
        return categoryRepository.select();
    }

    @Override
    public int DeletedCategoryById(int id) {
        return categoryRepository.DeletedCategoryById(id);
    }

    @Override
    public int UpdateCategory(int id, Category category) {
        return categoryRepository.UpdateCategory(id, category);
    }
}
