package com.example.restfull_HW.service.Imp;

import com.example.restfull_HW.respository.BookRepository;
import com.example.restfull_HW.respository.Model.Book;
import com.example.restfull_HW.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @Override
    public Book insert(Book book) {
        boolean isInserted = bookRepository.insert(book);
        if(isInserted){
            return book;
        }else{
            return null;
        }
    }

    @Override
    public List<Book> select() {
        return bookRepository.select();
    }

    @Override
    public int DeleteBook(int id) {
        return bookRepository.DeleteBook(id);
    }

    @Override
    public int UpdateBookById(int id, Book bookModel) {
        return bookRepository.UpdateBookById(id,bookModel);
    }

    @Override
    public List<Book> filterByTitle(String title) {
        return bookRepository.filterByTitle(title);
    }

    @Override
    public List<Book> FindById(int id) {
        return bookRepository.FindById(id);
    }
}
