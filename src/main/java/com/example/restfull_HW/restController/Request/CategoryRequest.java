package com.example.restfull_HW.restController.Request;

public class CategoryRequest {
    private String title;

    public CategoryRequest() {

    }

    public CategoryRequest(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoryRequest{" +
                "title='" + title + '\'' +
                '}';
    }
}
