package com.example.restfull_HW.restController.Request;

import com.example.restfull_HW.respository.Model.Category;

public class BookRequest {
    private String title;
    private String author;
    private String thumbnail;
    private String description;
    Category category;

    public BookRequest() {

    }

    public BookRequest(String title, String author, String thumbnail, String description, Category category) {
        this.title = title;
        this.author = author;
        this.thumbnail = thumbnail;
        this.description = description;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "BookRequest{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", description='" + description + '\'' +
                ", category=" + category +
                '}';
    }
}
