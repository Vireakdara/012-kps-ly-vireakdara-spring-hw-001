package com.example.restfull_HW.restController;

import com.example.restfull_HW.respository.Model.Category;
import com.example.restfull_HW.restController.Request.CategoryRequest;
import com.example.restfull_HW.restController.Response.APIResponse;
import com.example.restfull_HW.restController.Response.ResponseMessage;
import com.example.restfull_HW.service.Imp.CategoryServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
public class CategoryRestController {
    private CategoryServiceImp categoryService;

    @Autowired
    public void setCategoryService(CategoryServiceImp categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/category/post")
    public ResponseEntity<APIResponse<CategoryRequest>> insert(@RequestBody CategoryRequest categoryRequestModel){

        APIResponse<CategoryRequest> responseAIP = new APIResponse<>();
        ModelMapper modelMapper = new ModelMapper();

        Category categoryModel = modelMapper.map(categoryRequestModel,Category.class);
        Category result = categoryService.insert(categoryModel);

        CategoryRequest  response = modelMapper.map(result,CategoryRequest.class);

        responseAIP.setMessage("You have add book successfully");
        responseAIP.setData(response);
        responseAIP.setStatus(HttpStatus.OK);
        responseAIP.setTimes(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(responseAIP);
    }
    @GetMapping("/category/select")
    public ResponseEntity<APIResponse<List<Category>>> select(){
        ModelMapper modelMapper = new ModelMapper();
        APIResponse<List<Category>> response = new APIResponse<>();

        List<Category> resultQuery = categoryService.select();

        response.setMessage("You have found all Category successfully");
        response.setData(resultQuery);
        response.setStatus(HttpStatus.OK);
        response.setTimes(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/category/delete/{id}")
    public ResponseEntity<ResponseMessage>  deleteById(@PathVariable int id){

        ResponseMessage responseMessage = new ResponseMessage();

        if(!"".equals(id)){
            categoryService.DeletedCategoryById(id);
            responseMessage.setMessage("You deleted successfully");
            responseMessage.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(responseMessage);
        }else{
            responseMessage.setMessage("You false to delete!");
            responseMessage.setStatus(HttpStatus.BAD_REQUEST);
            return ResponseEntity.ok(responseMessage);
        }

    }
    @PutMapping("/category/update/{id}")
    public ResponseEntity<ResponseMessage> Update(@PathVariable int id, @RequestBody CategoryRequest categoryRequest){
        ResponseMessage responseMessage = new ResponseMessage();
        ModelMapper modelMapper = new ModelMapper();
        Category categoryModel = modelMapper.map(categoryRequest,Category.class);
        if(!("".equals(id)||"".equals(categoryModel))){
            categoryService.UpdateCategory(id,categoryModel);
            responseMessage.setMessage("You Update successfully");
            responseMessage.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(responseMessage);
        }else{
            responseMessage.setMessage("You Invalid to Update!");
            responseMessage.setStatus(HttpStatus.BAD_REQUEST);
            return ResponseEntity.ok(responseMessage);
        }
    }
}
