package com.example.restfull_HW.restController;

import com.example.restfull_HW.respository.Model.Book;
import com.example.restfull_HW.restController.Request.BookRequest;
import com.example.restfull_HW.restController.Response.APIResponse;
import com.example.restfull_HW.restController.Response.ResponseMessage;
import com.example.restfull_HW.service.Imp.BookServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BookRestController {
        private BookServiceImp bookService;

        @Autowired
        public void setBookService(BookServiceImp bookService) {
            this.bookService = bookService;
        }

        @PostMapping("/book/post")
        public ResponseEntity<APIResponse<BookRequest>> insert(@RequestBody BookRequest bookRequestModel){

            System.out.println(bookRequestModel);
            APIResponse<BookRequest> responseAIP = new APIResponse<>();

            ModelMapper modelMapper = new ModelMapper();
            Book book = modelMapper.map(bookRequestModel,Book.class);
            Book result = bookService.insert(book);

            BookRequest response = modelMapper.map(result,BookRequest.class);
            responseAIP.setMessage("You have add book successfully");
            responseAIP.setData(response);
            responseAIP.setStatus(HttpStatus.OK);
            responseAIP.setTimes(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(responseAIP);
        }

        @GetMapping("/book/select")
        public ResponseEntity<APIResponse<List<Book>>> select(){
            ModelMapper modelMapper = new ModelMapper();
            APIResponse<List<Book>> response = new APIResponse<>();
            List<Book> resultQuery = bookService.select();
            response.setMessage("You have found all Book successfully");
            response.setData(resultQuery);
            response.setStatus(HttpStatus.OK);
            response.setTimes(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(response);
        }

        @DeleteMapping("/book/delete/{id}")
        public ResponseEntity<ResponseMessage> delete(@PathVariable int id){
            ResponseMessage responseMessage = new ResponseMessage();
            if(!"".equals(id)){
                bookService.DeleteBook(id);
                responseMessage.setMessage("You delete successfully");
                responseMessage.setStatus(HttpStatus.OK);
                return ResponseEntity.ok(responseMessage);
            }else{
                responseMessage.setMessage("You delete fail !");
                responseMessage.setStatus(HttpStatus.OK);
                return ResponseEntity.ok(responseMessage);
            }
        }

        @PutMapping("/book/update/{id}")
        public ResponseEntity<ResponseMessage> Update(@PathVariable int id, @RequestBody BookRequest bookRequest){
            ResponseMessage responseMessage = new ResponseMessage();
            ModelMapper modelMapper = new ModelMapper();
            Book bookModel = modelMapper.map(bookRequest,Book.class);
            if(!("".equals(id)||"".equals(bookModel))) {
                bookService.UpdateBookById(id, bookModel);
                responseMessage.setMessage("You Update successfully");
                responseMessage.setStatus(HttpStatus.OK);
                return ResponseEntity.ok(responseMessage);
            }else{
                responseMessage.setMessage("You Invalid to Update!");
                responseMessage.setStatus(HttpStatus.BAD_REQUEST);
                return ResponseEntity.ok(responseMessage);
            }
        }



        @GetMapping("/book/search")
        public ResponseEntity<APIResponse<List<Book>>> filterByTitle(@RequestParam(required = false) String title){

            ModelMapper modelMapper = new ModelMapper();
            APIResponse<List<Book>> response = new APIResponse<>();

            List<Book> resultQuery = bookService.filterByTitle(title);
            response.setMessage("You have found by Book title");
            response.setData(resultQuery);
            response.setStatus(HttpStatus.OK);
            response.setTimes(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(response);
        }

        @GetMapping("/book/find")
        public ResponseEntity<APIResponse<List<Book>>> findById(@RequestParam(required = false) int id) {

            ModelMapper modelMapper = new ModelMapper();
            APIResponse<List<Book>> response = new APIResponse<>();

            List<Book> resultQuery = bookService.FindById(id);
            response.setMessage("You have found by Book title");
            response.setData(resultQuery);
            response.setStatus(HttpStatus.OK);
            response.setTimes(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(response);
        }

}
