package com.example.restfull_HW.restController.Response;

import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class APIResponse<T> {
    private String message;
    private T data;
    private HttpStatus status;
    private Timestamp times;

    public APIResponse(){

    }

    public APIResponse(String message, T data, HttpStatus status, Timestamp times) {
        this.message = message;
        this.data = data;
        this.status = status;
        this.times = times;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Timestamp getTimes() {
        return times;
    }

    public void setTimes(Timestamp times) {
        this.times = times;
    }

    @Override
    public String toString() {
        return "APIResponse{" +
                "message='" + message + '\'' +
                ", data=" + data +
                ", status=" + status +
                ", times=" + times +
                '}';
    }
}
