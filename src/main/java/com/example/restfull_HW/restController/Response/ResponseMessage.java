package com.example.restfull_HW.restController.Response;

import org.springframework.http.HttpStatus;

public class ResponseMessage {
    private String Message;
    private HttpStatus status;

    public ResponseMessage() {
    }

    public ResponseMessage(String message, HttpStatus status) {
        Message = message;
        this.status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ReponseMeassge{" +
                "Message='" + Message + '\'' +
                ", status=" + status +
                '}';
    }
}

